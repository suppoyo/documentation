# Dokumentation Suppoyo

Ausführlichere Dokumentation wird im [Wiki](https://gitlab.com/suppoyo/documentation/-/wikis/home) hinterlegt, u. a. die User Story Map.

## Konzept

* [Konzept: Suppoyo (Google Doc)](https://docs.google.com/document/d/1UFnz3mTIwFufmgz0gtMyle8E2y6GqhnOChTpCJgmdWc/edit#)
* [Konzept: Einkaufshilfe für hilfsbedürftige und ältere Menschen](https://docs.google.com/document/d/13l_YbCvsapdiFmeFVVPbb-kUa65pZTtJAujZfmtoaZM/edit#)

## Technologie 

Backend: Node.js
Datenbank: Mysql
API: Rest (Jason)

### weitere Ideen

Idee für Datenbankmapping: [TypeORM](https://github.com/typeorm/typeorm)